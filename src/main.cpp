
#include <iostream>
#include "a/a.h"
#include "e/e.h"

using namespace std;

int main(int argc, char*argv[]) {
  a::doThingA();
  e::doThingE();
}
